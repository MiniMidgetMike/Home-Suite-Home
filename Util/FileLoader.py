import os, re
from os.path import isfile, join, exists
from functools import wraps

""" NOTE: ANY FILE SAVING WILL OVERWRITE EMPTY FILES """

def openFile(file, mode='r+'):
    try:
        f = open(file, mode)
        return f
    except IOError as e:
        if (isfile(file)):
            raise (e, "*** FILE EXISTS ***")
        else:
            raise (e)

def readFile(file):
    if not fileExists(file):
        return None,1
    with openFile(file, mode='r') as f:
        try:
            st = f.read()
            return st,0
        except Exception as e:
            print (F"Encountered error!\n{e}")
            return None,1


def fileEmpty(name):
    return not (fileExists(name) and os.path.getsize(name) > 0)

def fileExists(name):
    return isfile(name)

def validLoc(loc): # works for directories and files
    return exists(loc)

def eraseFile(file):
    try:
        file = openFile(file, mode="w")
        file.truncate(0)
        file.close()
    except IOError as e:
        print (e)

def writeList(file, lst):
    try:
        file.write("\n".join([str(i) for i in lst]))
        file.write("\n")
        file.close()
    except IOError as e:
        raise (e)

def writeObj(file, obj, mode='w'): # write obj to file, overwriting if file exists
    with openFile(file, mode=mode) as f:
        f.write(obj)

def log(logfile='log.txt'):
    def logit(func):
        @wraps(func)
        def logged(*args, **kwargs):
            eraseFile(logfile)
            out = func(*args, **kwargs)
            with open(logfile, 'a') as f:
                f.write(func.__name__ +  ": [{}, {}]\n".format(*args, *kwargs))
                f.write(out)
            return out
        return logged
    return logit

def saveFile(file, obj): # write obj to file, if file exists, ask if it should be overwritten
                         # if no, append '(n)' to file prefix, where 'n' is a number
    mode = 'r+' if fileExists(file) else 'w'
    num = 1

    overwrite = False
    
    if (not fileEmpty(file)):
        overwrite = input("File exists, overwrite? [y | n]:\n").casefold()
        while (overwrite != 'y' and overwrite != 'n'):
            print ("Please answer with 'y' or 'n'\n")
            overwrite = input("File exists, overwrite? [y | n]:\n").casefold()
        if (overwrite == 'y'):
            overwrite = True
        else:
            overwrite = False

    prefix = file[:file.find('.')]
    suffix = file[file.find('.'):]
    while (not fileEmpty(file)) and (not overwrite):
        file = prefix + F"({num})" + suffix # append '(n)' to prefix of file (i.e. 'file.txt' -> 'file(1).txt')
        print (file)
        mode = 'w'
        num+=1
    else:
        if overwrite: print ("\noverwriting...\n")

    try:
        if fileExists(file):
            eraseFile(file)
        writeObj(file, obj, mode=mode)
    except Exception as e:
        print (F'Could not be saved, encountered error:\n"{e}"')
    else:
        print ("Saved!\n")
