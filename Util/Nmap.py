import subprocess as sp
import webbrowser, os, sys, FileLoader as fl
from multiprocessing import Process
sys.path.append("../")

"""
TO-DO:
    - Add support for *nix
    - Add support for saving nmap scan results in different file types (i.e. xml)
    - Finish
    - Git good
"""

EXEC = ""
PROCS = []

def getDir():
    EXEC = checkDir()[0]
    if EXEC == None: # file was empty / file doesn't exist / exit code was 1
        EXEC = input("Please give nmap directory:\n")
    
    while not fl.validLoc(EXEC) or fl.fileExists(EXEC): # is not an existing directory / is a file
        print (F"{EXEC} is not a valid directory (NOTE: Only give directory of executable, not file)\n")
        EXEC = input("Please give nmap directory:\n")
    
    if sys.platform == 'win32':
        if not (fl.fileExists(EXEC + "nmap.exe")):
            print (F"{EXEC} does not contain nmap.exe, please give the nmap install directory")
            getDir()
        else:
            fl.writeObj("exec-dir", EXEC)
            return EXEC

def checkDir():
    if not fl.fileEmpty("exec-dir") and fl.fileExists("exec-dir"):
        test = fl.readFile("exec-dir")
        if test[1] == 1 or test[0] == None:
            return test
        else:
            return test
    else:
        return

def run():
    choice = input("""What would you like to do? \n [R]un Zenmap \n [N]map scan \n [H]elp \n [C]lear \n [E]nd \n""").casefold()
    options = ("s", "h", "e", "r", "c", "n",)

    while choice not in options:
        print (F"{choice} is not a valid answer\n")
        choice = input("""What would you like to do? \n [R]un Zenmap \n [N]map scan \n [H]elp \n [C]lear \n [E]nd \n""").casefold()

    if choice == 'r':
        PROCS.append(["zenmap.exe", zmap()])
        run()
    elif choice == 'n':
        argString = input("Nmap arguments:\n").split()
        args = []
        
        for n,a in enumerate(argString): # check for ['-[-]arg', 'value']
            if n != len(argString) - 1:
                if a[0] == '-' and argString[n+1][0] != '-' and ('.' not in argString[n+1] and argString[n+1] != 'localhost'): # ['-[-]arg', 'value']
                    args.append(' '.join([a, argString[n+1]]))
                elif a[0] == '-' or a == 'localhost':
                    args.append(a)
            elif a[0] == '-' or (a not in args):
                args.append(a)

        nmap(args)
        run()
    elif choice == 'h':
        nHelp()
        run()
    elif choice == 'c':
        print ("\n" * 50)
        run()
    else: # choice == 'e'
        alive = liveProcs()
        if alive != None:
            print (F"There are running processes:\n{alive}\n")
            print ("Please close running processes or wait for them to finish\n")
            run()
            return
            
        sure = input("Are you sure? [y/n]:\n").casefold()

        while sure != 'y' and sure != 'n':
            print ("Please answer with 'y' or 'n'")
            sure = input("Are you sure? [y/n]:\n").casefold()

        if sure == 'y':
            return
        else:
            run()

@fl.log('nmap.log')
def runCmd(cmd, *args, **kwargs):
    sh = [EXEC + cmd]
    sh.extend(list(*args))
    sh.extend(list(*kwargs))
    
    out = sp.run(sh, shell=True, stdout=sp.PIPE)#, check=True)
    return  out.stdout.rstrip().decode()


def nmap(*args):
    plat = sys.platform
    if plat == 'win32':
        output = runCmd("nmap.exe", *args)
        print (output + "\n")
        
        save = input("Save results? [y/n]:\n").casefold()
        while save != 'y' and save != 'n':
            print ("Answer with 'y' or 'n'")
            save = input("Save results? [y/n]:\n").casefold()

        if save == 'y':
            file = input("File to save to:\n").casefold()
            fl.saveFile(file, output)

    else: # not yet implemented
        return

def zmap():
    plat = sys.platform

    if plat == 'win32':
        try:
            p = Process(target=sp.run, args=(EXEC + "zenmap.exe",))
            p.start()
            print ("Starting Zenmap...\n")
            return p
        except Exception as e:
            print (F"Encountered error:\n{e}")
    else:
        runCmd("zenmap")

def liveProcs():
    alive = []
    for p in PROCS:
        if p[1].is_alive():
            alive.append(p[0])
    if len(alive) > 0:
        return alive
    return None

def nHelp():
    hlp = input('Which page? \n [W]iki page \n [N]map website \n Nmap --[H]elp \n [E]xit \n').casefold()
    options = ['w','n','h','e']

    while hlp not in options:
        print(F"{hlp} is not a valid response")
        hlp = input('Which page? \n [W]iki page \n [N]map website \n Nmap [H]elp \n [E]xit \n').casefold()

    if hlp == 'w':
        webbrowser.open("https://en.wikipedia.org/wiki/Nmap")
        return
    elif hlp == 'n':
        webbrowser.open("https://nmap.org/book/man-briefoptions.html")
        return
    elif hlp == 'h':
        nmap("--help")
        return
    else: # hlp == 'e'
        return


if __name__ == "__main__":
    EXEC = getDir()
    run()


        
