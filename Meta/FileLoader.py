import os, re
from os.path import isfile, join

def openFile(file, mode='r+'):
    if (mode[0] == 'w' or mode[len(mode)-1] == '+'):
        try:
            f = open(file, mode)
            return f
        except IOError as e:
            if (isfile(file)):
                raise (e, "*** FILE EXISTS ***")
            else:
                raise (e)
    else:
        f = open(file, mode)
        return f

def fileEmpty(name):
    return False if (fileExists(name) and os.path.getsize(name) > 0) else True

def fileExists(name):
    return True if (isfile(name)) else False

def eraseFile(file):
    try:
        m = file.mode
        name = file.name
        file.truncate(0)
        file.close()
        return openFile(name, m)
    except IOError as e:
        rase (e)

def writeList(file, lst):
    try:
        file.write("\n".join([str(i) for i in lst]))
        file.write("\n")
        file.close()
    except IOError as e:
        raise (e)
