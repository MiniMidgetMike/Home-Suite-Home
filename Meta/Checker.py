import sys
from pprint import pprint
sys.path.append("../")
import io, FileLoader as fl
from pprint import pprint

def check(*bChars, file='test.exe', encoder='unicode-escape'):

    try:
        B = [bytes(b) for b in bChars] # [b'\x00', b'\xff', ...]
        pprint(B)
    except Exception as e:
        raise(e)

    bad = {k:False for k in B}

    with open(file, 'rb') as f:
        pl = f.read()
        for b in B:
            #print (b)

            if b in pl:
                print (F"BAD CHARACTER FOUND: {b}")
                print (*[bytes.fromhex(hex(i)[2:].zfill(2)) for i in (pl[0:pl.index(b)+4])])
                bad[b] = True

    #pprint(bad)
    return ([k for k,v in bad.items() if v], (True in bad.values()))


if __name__ == "__main__":
    bad = check(b"\x00", b"\xff")
    print (bad)
    good = check(b"\x00", b"\xff")
    print (good)
