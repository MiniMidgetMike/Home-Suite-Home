from itertools import combinations as comb, permutations as perm
import math, random, sys, re, itertools as it
from pprint import pprint
sys.path.append("../")
from Util import FileLoader as fl
"""
                                                DictGEN
    *---------------------------------------------------------------------------------------------------*
    | A python program designed to generate a password dictionary based on keywords and character types |
    *---------------------------------------------------------------------------------------------------*
"""


"""
    NOTES TO SELF / TO-DO LIST :
        - Get of your ass and finish the character-replacement dictionary (is done?)
        - Git good
"""

class DictGEN():
    
    def __init__(self, kwords, cType="an", pairs=2, file="dictionary.txt", debug=False, _pass=None):
                             # cType options: 'all', 'an' (alpha-numeric), 'a' (alpha)


        self.debug = debug if (file is not None) else False 
        self._pass = _pass
        self.file = file
        self.kwords = set(kwords)
        self.charType = cType.casefold()
        self.dict = []
        self.pairs = pairs
        cMatches = { # Now comes the annoyingly-long section of code...
            'an': { # alpha-numeric
                'a': ['4', 'A', 'a'], # Use lists for multiple matches (i.e. 's' : ['5', '$'])
                'b': ['6', '8', 'B', 'b'],
                'c': ['C', 'c'],
                'd': ['D', 'd'],
                'e': ['3', 'E', 'e'],
                'f': ['F', 'f'],
                'g': ['9', 'G', 'g'],
                'h': ['H', 'h'],
                'i': ['1', 'I', 'i'],
                'j': ['J', 'j'],
                'k': ['K', 'k'],
                'l': ['1', 'L', 'l'],
                'm': ['M', 'm'],
                'n': ['N', 'n'],
                'o': ['0', 'O', 'o'],
                'p': ['P', 'p'],
                'q': ['Q', 'q'],
                'r': ['R', 'r'],
                's': ['5', 'S', 's'],
                't': ['7', 'T', 't'],
                'u': ['U', 'u'],
                'v': ['V', 'v'],
                'w': ['W', 'w'],
                'x': ['X', 'x'],
                'y': ['Y', 'y'],
                'z': ['2', 'Z', 'z'],
                '0': ['0', 'o', 'O'],
                '1': ['1', 'l', 'L', 'i', 'I'],
                '2': ['2', 'Z', 'z'],
                '3': ['3', 'e', 'E'],
                '4': ['4', 'a', 'A'],
                '5': ['5', 's', 'S'],
                '6': ['6', 'b', 'B'],
                '7': ['7', 't', 'T'],
                '8': ['8', 'b', 'B'],
                '9': ['9', 'g', 'G']},
            'all': { # all character types
                'a': ['4', '@', 'A', 'a'],
                'b': ['6', '8', 'B', 'b'],
                'c': ['C', 'c'],
                'd': ['D', 'd'],
                'e': ['3', 'E', 'e'],
                'f': ['F', 'f'],
                'g': ['G', 'g'],
                'h': ['H', 'h'],
                'i': ['1', 'I', 'i'],
                'j': ['J', 'j'],
                'k': ['K', 'k'],
                'l': ['1', '|', 'L', 'l'],
                'm': ['M', 'm'],
                'n': ['N', 'n'],
                'o': ['0', 'O', 'o'],
                'p': ['P', 'p'],
                'q': ['Q', 'q'],
                'r': ['R', 'r'],
                's': ['5', '$', 'S', 's'],
                't': ['7', '+', 'T', 't'],
                'u': ['U', 'u'],
                'v': ['V', 'v'],
                'w': ['W', 'w'],
                'x': ['X', 'x'],
                'y': ['Y', 'y'],
                'z': ['2', 'Z', 'z'],
                '0': ['0', 'o', 'O'],
                '1': ['1', '|', 'l', 'L', 'i', 'I'],
                '2': ['2', 'Z', 'z'],
                '3': ['3', 'e', 'E'],
                '4': ['4', 'a', 'A'],
                '5': ['5', 's', 'S'],
                '6': ['6', 'b', 'B'],
                '7': ['7', 't', 'T'],
                '8': ['8', 'b', 'B'],
                '9': ['9', 'g', 'G'],
                '@': ['@', 'A', 'a'],
                '|': ['|', '1', 'L', 'l'],
                '$': ['$', '5', 'S', 's'],
                '+': ['+', 'T', 't']}, # Not yet finished (?)
            'a': {} # alpha
        } # ...and done
        for i in range(32,127): # adds all printable (and allowed) characters
            i = chr(i).casefold()
            chars = cMatches['all']
            values = list (it.chain(*cMatches['all'].values()))
            if not (i in list(chars.keys())):
                cMatches['all'].setdefault(i,[i] if i not in values else [i.capitalize(),i])


        for c in cMatches['an']: # adds alpha characters
            try: # break if c is int
                int(c)
                break
            except:
                cMatches['a'][c] = cMatches['an'][c]
                while len(re.findall('[0-9]', ''.join(cMatches['a'][c]))) > 0: # match includes numeric characters
                    for k,v in cMatches['a'].items():
                        for i in range(10):
                            try:
                                v.pop(v.index(str(i))) # remove numeric characters
                            except:
                                pass



        self.cMatches = cMatches
        del cMatches

    def genPerms(self):
        words = self.kwords
        if self.debug: print ("words:",words)
        pList = []

        for w in words:
            pList.extend(self._wordPerms(w))

        #print ("pList:",pList) # not a good idea...
        pList = [''.join(i) for i in pList]

        pList = self.genDict(pList)
        
        self.dict = pList
        
        if self.file != None:
            self._saveDict()
        else:
            print ("\n".join([str(i) for i in self.dict])) # Only output if file is None; can be used for piping output
        
        
    def _wordPerms(self, word): # returns a list of all permutations of character swaps in word
        chars = self.cMatches
        word = tuple(word)
        cType = self.charType
        reps = [ chars[cType][i] for i in word ] # [ [a1,a2,...,aN], [b1,b2,...,bN], ... ]
                                                 # where '[a1..aN]' & '[b1..bN]' are all possible replacements of the corresponding letters (a,b) in word
        if self.debug: print ("reps:",reps)

        perms = set(it.product(*reps)) # TA-DA! thanks, distribution!

        #pprint(perms)
        return perms

    def genDict(self, words):

        return list(it.chain([''.join(i) for i in perm(words, self.pairs)],['_'.join(i) for i in perm(words, self.pairs)]))


    def _saveDict(self):
        file = self.file
        dictionary = self.dict

        print ('\n' * 50)
        
        if (self.debug):
            print (F"LENGTH: {len(dictionary)}")
            print (F"SIZE: {sys.getsizeof(dictionary)}")
            if (self._pass in dictionary):
                print ("SUCCESS")
            else:
                print ("FAILURE")

        fl.saveFile(file, dictionary)
        
        

    
class DGRunner():
    def __init__(self, kwords, **args): # passes parameters to DG

        assert ((type(kwords) == list or type(kwords) == tuple) and len(kwords) > 0), "Please give a non-empty list of keywords"
        if 'cType' in args:
            cType = args['cType']
            assert (cType.casefold() in ['an', 'all', 'a']), "Character type must be one of 'all', 'an' (alpha-num), 'a' (alpha)"
        if 'debug' in args and '_pass' in args:
            debug, _pass = args['debug'], args['_pass']
            assert ((not debug and _pass==None) or (debug and _pass!=None)), "Password must be given only when debug == True"
        
        self.dg = DictGEN(kwords, **args)
        self.gen()

    def gen(self):
        dg = self.dg
        dg.genPerms()








if __name__ == "__main__":
    dgr = DGRunner(['1337', 'haxxor', 'pwned', 'pwn', 'rekt', 'noob'], cType='all', file="test.txt", debug=True, _pass='|337_H@xx0r')
